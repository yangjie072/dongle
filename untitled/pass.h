#ifndef PASS_H
#define PASS_H
#include <QDialog>
#include <QLabel>
#include <QLineEdit>
#include <QPushButton>

class pass : public QDialog
{
public:
    pass(QWidget *parent = Q_NULLPTR, Qt::WindowFlags f = Qt::WindowFlags());

    bool getLoginStat() const
    {
        return passwdSuccessed2;
    }

private:
    QLabel *lb_pass12;
    //QLineEdit *le_user;
    QLineEdit *le_passwd2;
    QPushButton *bt_login12;

    bool passwdSuccessed2;
};


#endif // PASS_H
