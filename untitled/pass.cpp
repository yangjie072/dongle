#include "pass.h"
#include <QVBoxLayout>
#include <QMessageBox>

pass::pass(QWidget *parent, Qt::WindowFlags f)
    :QDialog(parent, f)
{
    passwdSuccessed2 = false;
    lb_pass12 = new QLabel("请输入正确的密钥");
    lb_pass12->setAlignment(Qt::AlignCenter);

    bt_login12 = new QPushButton("确定");
    //bt_login->setEnabled(false);

    QVBoxLayout *vbox = new QVBoxLayout;
    vbox->addWidget(lb_pass12);
    vbox->addWidget(bt_login12);
    setLayout(vbox);

    connect(bt_login12, &QPushButton::clicked, [&](){
            close();
    });

}
