#ifndef DECODE_H
#define DECODE_H
#include <QDialog>
#include <QLabel>
#include <QLineEdit>
#include <QPushButton>

class decode : public QDialog
{
public:
    decode(QWidget *parent = Q_NULLPTR, Qt::WindowFlags f = Qt::WindowFlags());

    bool getLoginStat() const
    {
        return loginSuccessed;
    }

private:
    QLabel *lb_logo;
    //QLineEdit *le_user;
    QLineEdit *le_pass;
    QPushButton *bt_login;

    bool loginSuccessed;
};


#endif // DECODE_H
