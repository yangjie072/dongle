#ifndef LOGIN_H
#define LOGIN_H
#include <QDialog>
#include <QLabel>
#include <QLineEdit>
#include <QPushButton>
#include <QTcpSocket>
class login : public QDialog
{
public:
    login(QWidget *parent = Q_NULLPTR, Qt::WindowFlags f = Qt::WindowFlags());

    bool getLoginStat() const
    {
        return loginSuccessed;
    }
    QString getUserName() const
    {
        return le_user->text();
    }
private slots:
    void read_data();
    void on_pushButton_clicked();
private:
    QLabel *lb_logo;
    QLineEdit *le_user;
    QLineEdit *le_pass;
    QPushButton *bt_login;
    QPushButton *bt_regiser;
    QTcpSocket *loginsock;
    bool loginSuccessed;
};

#endif // LOGIN_H
