#ifndef DECODE1_H
#define DECODE1_H
#include <QDialog>
#include <QLabel>
#include <QLineEdit>
#include <QPushButton>

class decode1 : public QDialog
{
public:
    decode1(QWidget *parent = Q_NULLPTR, Qt::WindowFlags f = Qt::WindowFlags());

    bool getLoginStat() const
    {
        return passwdSuccessed1;
    }

private:
    QLabel *lb_pass11;
    //QLineEdit *le_user;
    QLineEdit *le_passwd1;
    QPushButton *bt_login11;

    bool passwdSuccessed1;
};

#endif // DECODE1_H
