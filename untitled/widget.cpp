#include "login.h"
#include "passwd.h"
#include "decode1.h"
#include "ui_widget.h"
#include "decode.h"
#include "pass.h"
#include "widget.h"
#include <QFile>
#include <QFileDialog>
#include <QDesktopServices>
#include <QUrl>

Widget::Widget(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::Widget)
{
    ui->setupUi(this);
    ui->lineEdit_2->setPlaceholderText("请输入密钥");

}
Widget::~Widget()
{
    delete ui;
}
//选择文件
void Widget::on_pushButton_clicked()
{
    filename = QFileDialog::getOpenFileName();
    ui->lineEdit->setText(filename);
    ui->label_2->setText(filename);
}
void Widget::on_pushButton_8_clicked()
{

    close();
}
//加密算法
void Widget::on_pushButton_7_clicked()
{
     passwd *pw=new passwd;
    if(ui->lineEdit_2->text().isEmpty()){
        pw->exec();
        return;
    }else
    {
        ui->label_4->setText(" ");
        ui->label_6->setText(" ");
        //1.读写打开文件
        QFile f(filename);
        if(!f.open(QIODevice::ReadWrite))
        {
            ui->label_4->setText("请选择加密文件！");
            ui->label_4->setStyleSheet("color:red;");
            return;
        }
        QString pass;
        QByteArray buf = f.readAll();
        ui->progressBar->setRange(0, buf.length()-1);
        QString str=ui->textBrowser_2->toPlainText();
        if(str=="加密算法1")
        {
            pass = ui->lineEdit_2->text();
            ui->label_4->setText(" ");
            if(buf.endsWith("jiamisuanfa1"))
            {
                ui->label_4->setText("该文件已使用加密算法一加密");
                ui->label_4->setStyleSheet("color:red;");
                return;
            }else{
                for(int i=0; i<buf.length(); i++)
                {
                    buf[i] = ~buf[i];
                    //ui->progressBar->setValue(i);
                }
                if(buf.endsWith("jiamisuanfa1"))
                {
                    ui->label_4->setText("该文件已使用加密算法一加密");
                    return;
                }else{
                    for(int i=0; i<buf.length(); i++)//还原
                    {
                        buf[i] = ~buf[i];
                        //ui->progressBar->setValue(i);
                    }
                    buf.append(pass);//添加密钥
                    buf.append("jiamisuanfa1");//添加识别证书

                    for(int i=0; i<buf.length(); i++)
                    {
                        buf[i] = ~buf[i];
                        ui->progressBar->setValue(i);
                    }
                }
            }
        }else if(str=="加密算法2"){
            pass = ui->lineEdit_2->text();
            ui->label_4->setText(" ");
            if(buf.endsWith("jiamisuanfa2"))
            {
                ui->label_4->setText("该文件已使用加密算法二加密");
                ui->label_4->setStyleSheet("color:red;");
                return;
            }else{
                for(int i=0; i<buf.length(); i++)
                {
                    buf[i] = buf[i]-10;
                    //ui->progressBar->setValue(i);
                }
                if(buf.endsWith("jiamisuanfa2"))
                {
                    ui->label_4->setText("该文件已使用加密算法二加密");
                    return;
                }else{
                    for(int i=0; i<buf.length(); i++)//还原
                    {
                        buf[i] = buf[i]+10;
                        //ui->progressBar->setValue(i);
                    }
                    buf.append(pass);//添加密钥
                    buf.append("jiamisuanfa2");//添加识别证书
                    for(int i=0; i<buf.length(); i++)
                    {
                        buf[i] = buf[i]+10;
                        ui->progressBar->setValue(i);
                    }
                }
            }
        }else if(str=="加密算法3"){
            pass = ui->lineEdit_2->text();
            ui->label_4->setText(" ");
            if(buf.endsWith("jiamisuanfa3"))
            {
                ui->label_4->setText("该文件已使用加密算法三加密");
                ui->label_4->setStyleSheet("color:red;");
                return;
            }else{
                for(int i=0; i<buf.length(); i++)
                {
                    buf[i] = ~(buf[i]-'x');
                    //ui->progressBar->setValue(i);
                }
                if(buf.endsWith("jiamisuanfa3"))
                {
                    ui->label_4->setText("该文件已使用加密算法三加密");
                    return;
                }else{
                    for(int i=0; i<buf.length(); i++)//还原
                    {
                        buf[i] = ~buf[i]+'x';
                        //ui->progressBar->setValue(i);
                    }
                    buf.append(pass);//添加密钥
                    buf.append("jiamisuanfa3");//添加识别证书
                    for(int i=0; i<buf.length(); i++)
                    {
                        buf[i] = ~buf[i]+'x';
                        ui->progressBar->setValue(i);
                    }
                }
            }
        }else{
            ui->label_6->setText("请选择加密算法！");
            ui->label_6->setStyleSheet("color:red;");
            return;
        }
        ui->lineEdit_2->setText("");
        //4.回写
        f.seek(0);
        f.write(buf);
        //5.关闭
        f.close();
        ui->label_4->setText("加密成功");
    }
}
//解密算法
void Widget::on_pushButton_9_clicked()
{

//    if(flag!="1")
//    {
//        decode *d=new decode;
//        d->exec();
//        flag = "1";
//        if(!d->getLoginStat())
//            flag = "0";
//    }
    ui->label_4->setText(" ");
    ui->label_6->setText(" ");

    decode1 *de=new decode1;
    pass *pa=new pass;
   if(ui->lineEdit_2->text().isEmpty()){
       de->exec();
       return;
   }else
   {
       //1.读写打开文件
       QFile f(filename);
       if(!f.open(QIODevice::ReadWrite))
       {
           ui->label_4->setText("请选择解密文件！");
           ui->label_4->setStyleSheet("color:red;");
           return;
       }
       QByteArray buf = f.readAll();
       ui->progressBar->setRange(0, buf.length()-1);
       QString str=ui->textBrowser_2->toPlainText();
       QString pass = ui->lineEdit_2->text();
       if(str=="解密算法1")
       {
           ui->label_4->setText("");
           for(int i=0; i<buf.length(); i++)
           {
               buf[i] = ~buf[i];
               ui->progressBar->setValue(i);
           }
           //算法解密1
           if(buf.endsWith("jiamisuanfa1"))
           {
               buf.chop(12);
               if(buf.endsWith(pass.toLatin1()))
                   buf.chop(pass.length());
               else
               {
                   pa->exec();
                    ui->lineEdit_2->setText("");
                   return;
               }
           }else{
               ui->label_4->setText("该文件未使用加密算法一加密");
               ui->label_4->setStyleSheet("color:red;");
               return;
           }
       }else if(str=="解密算法2"){
           ui->label_4->setText("");
           for(int i=0; i<buf.length(); i++)
           {
               buf[i] = buf[i]-10;
               ui->progressBar->setValue(i);
           }
           //算法解密2
           if(buf.endsWith("jiamisuanfa2"))
           {
               buf.chop(12);
               if(buf.endsWith(pass.toLatin1()))
                   buf.chop(pass.length());
               else
               {
                   pa->exec();
                   ui->lineEdit_2->setText("");
                   return;
               }
           }else{
               ui->label_4->setText("该文件未使用加密算法二加密");
               ui->label_4->setStyleSheet("color:red;");
               return;
           }
       }else if(str=="解密算法3"){
           ui->label_4->setText("");
           for(int i=0; i<buf.length(); i++)
           {
               buf[i] = ~(buf[i]-'x');
               ui->progressBar->setValue(i);
           }
           //算法解密3
           if(buf.endsWith("jiamisuanfa3"))
           {
               buf.chop(12);
               if(buf.endsWith(pass.toLatin1()))
                   buf.chop(pass.length());
               else
               {
                   pa->exec();
                    ui->lineEdit_2->setText("");
                   return;
               }
            }else{
                   ui->label_4->setText("该文件未使用加密算法三加密");
                   ui->label_4->setStyleSheet("color:red;");
                   return;
           }
       }else{
           ui->label_6->setText("请选择解密算法！");
           ui->label_6->setStyleSheet("color:red;");
           return;
       }
       f.close();
       ui->lineEdit_2->setText("");
       f.open(QFile::WriteOnly|QFile::Truncate);
       //4.回写
       //f.seek(0);
       f.write(buf);
       //ui->progressBar->setValue(i);
       //5.关闭
       f.close();
       ui->label_4->setText("解密成功");
   }
}
//清空
void Widget::on_pushButton_4_clicked()
{
    ui->textBrowser_2->setText("");
}

//帮助按钮
void Widget::on_pushButton_6_clicked()
{
    QDesktopServices::openUrl(QUrl(QString("https://www.baidu.com")));
}

void Widget::on_pushButton_2_clicked()
{
    QDesktopServices::openUrl(QUrl(QString("https://www.baidu.com")));
}

void Widget::on_comboBox_3_activated(const QString &arg1)
{
    ui->textBrowser_2->setText(arg1);
}
