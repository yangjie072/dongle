#include "decode1.h"
#include <QVBoxLayout>
#include <QMessageBox>

decode1::decode1(QWidget *parent, Qt::WindowFlags f)
    :QDialog(parent, f)
{
    passwdSuccessed1 = false;
    lb_pass11 = new QLabel("请输入密钥");
    lb_pass11->setAlignment(Qt::AlignCenter);

    bt_login11 = new QPushButton("确定");
    //bt_login->setEnabled(false);

    QVBoxLayout *vbox = new QVBoxLayout;
    vbox->addWidget(lb_pass11);
    vbox->addWidget(bt_login11);
    setLayout(vbox);

    connect(bt_login11, &QPushButton::clicked, [&](){
            close();
    });

}

