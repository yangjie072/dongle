#include "decode.h"
#include <QVBoxLayout>
#include <QMessageBox>

decode::decode(QWidget *parent, Qt::WindowFlags f)
    :QDialog(parent, f)
{
    loginSuccessed = false;
    lb_logo = new QLabel("请联系管理员获取许可证 （123456）");
    lb_logo->setAlignment(Qt::AlignCenter);

    le_pass = new QLineEdit;
    le_pass->setPlaceholderText("请输入密钥");

    bt_login = new QPushButton("进入");
    //bt_login->setEnabled(false);

    QVBoxLayout *vbox = new QVBoxLayout;
    vbox->addWidget(lb_logo);
    vbox->addWidget(le_pass);
    vbox->addWidget(bt_login);
    setLayout(vbox);

    connect(bt_login, &QPushButton::clicked, [&](){
        if(!le_pass->text().isEmpty() && "123456" == le_pass->text())
        {
            loginSuccessed = true;
            close();
        }
        else
            QMessageBox::warning(this, "密码错误","请输入正确的许可证");
    });

}
