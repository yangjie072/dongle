#include "passwd.h"
#include <QVBoxLayout>
#include <QMessageBox>

passwd::passwd(QWidget *parent, Qt::WindowFlags f)
    :QDialog(parent, f)
{
    passwdSuccessed = false;
    lb_pass1 = new QLabel("请输入加密密钥");
    lb_pass1->setAlignment(Qt::AlignCenter);

    bt_login1 = new QPushButton("确定");
    //bt_login->setEnabled(false);

    QVBoxLayout *vbox = new QVBoxLayout;
    vbox->addWidget(lb_pass1);
    vbox->addWidget(bt_login1);
    setLayout(vbox);

    connect(bt_login1, &QPushButton::clicked, [&](){
            close();
    });

}
