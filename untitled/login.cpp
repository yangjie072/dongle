#include "login.h"
#include "widget.h"
#include <QVBoxLayout>
#include <QMessageBox>
#include <QSpacerItem>
#include <QDebug>
login::login(QWidget *parent, Qt::WindowFlags f)
    :QDialog(parent, f)
{

    loginsock = new QTcpSocket;
    //联通服务器
    loginsock->connectToHost("192.168.220.128", 6666);
    //connect(loginsock, SIGNAL(readyRead()), this, SLOT(read_data()));

    loginSuccessed = false;
    lb_logo = new QLabel("欢迎");
    //lb_logo->setFixedSize(100,100);
    lb_logo->setAlignment(Qt::AlignCenter);
    le_user = new QLineEdit;
    le_user->setPlaceholderText("用户名"); //显示提示信息
    le_user->setFixedSize(380,30);
    le_user->setAlignment(Qt::AlignCenter);
    le_pass = new QLineEdit;
    le_pass->setFixedSize(380,30);
    le_pass->setAlignment(Qt::AlignCenter);
    le_pass->setPlaceholderText("密码");
    le_pass->setEchoMode(QLineEdit::Password);
    bt_login = new QPushButton("登录");
    bt_login->setFixedSize(50,30);
    bt_regiser = new QPushButton("注册");
    bt_regiser->setFixedSize(50,30);
    //bt_login->setEnabled(false);
    QHBoxLayout *hbox = new QHBoxLayout;
    hbox->addWidget(bt_login);
    hbox->addWidget(bt_regiser);
    QVBoxLayout *vbox = new QVBoxLayout;
    vbox->addWidget(lb_logo);
    vbox->addWidget(le_user);
    vbox->addWidget(le_pass);
    vbox->addLayout(hbox);
    setLayout(vbox);
    //connect(bt_login, SIGNAL(clicked(bool)), this, SLOT(on_pushButton_clicked));
    connect(bt_login, &QPushButton::clicked, [&](){
        //QString st = "1";
        QString str1 = "#";
        //loginsock->write(st.toStdString().c_str());
        QString name = le_user->text();
        QString password = le_pass->text();
        loginsock->write(name.toStdString().c_str());
        loginsock->write(str1.toStdString().c_str());
        loginsock->write(password.toStdString().c_str());

        QByteArray buf = loginsock->readAll();
        if(buf=="sad")
        {
            loginSuccessed = true;
            close();
        }
        else
            QMessageBox::warning(this, "登陆异常","用户名或密码错误");
        });

}
//void login::on_pushButton_clicked()
//{
//    QString name = le_user->text();
//    QString password = le_pass->text();
//    loginsock->write(name.toStdString().c_str());
//    loginsock->write(password.toStdString().c_str());
//    qDebug()<<"sadasd";
//}
