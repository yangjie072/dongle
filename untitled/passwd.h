#ifndef PASSWD_H
#define PASSWD_H
#include <QDialog>
#include <QLabel>
#include <QLineEdit>
#include <QPushButton>

class passwd : public QDialog
{
public:
    passwd(QWidget *parent = Q_NULLPTR, Qt::WindowFlags f = Qt::WindowFlags());

    bool getLoginStat() const
    {
        return passwdSuccessed;
    }

private:
    QLabel *lb_pass1;
    //QLineEdit *le_user;
    QLineEdit *le_passwd;
    QPushButton *bt_login1;

    bool passwdSuccessed;
};

#endif // PASSWD_H
