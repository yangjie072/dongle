#ifndef WIDGET_H
#define WIDGET_H

#include <QWidget>
#include <QTcpSocket>

namespace Ui {
class Widget;
}

class Widget : public QWidget
{
    Q_OBJECT
//private slots:
//    void read_data();
//    void on_pushButton_clicked();

public:
    explicit Widget(QWidget *parent = 0);
    QString flag = "0";

    ~Widget();

private slots:
    void on_pushButton_clicked();


    void on_pushButton_8_clicked();

    void on_pushButton_7_clicked();

    //void on_comboBox_3_activated(const QString &arg1);

    void on_pushButton_9_clicked();

    void on_pushButton_4_clicked();

    void on_pushButton_6_clicked();

    void on_pushButton_2_clicked();

    void on_comboBox_3_activated(const QString &arg1);

private:
    Ui::Widget *ui;
    //QTcpSocket *mysock;
    QString filename;



};

#endif // WIDGET_H
